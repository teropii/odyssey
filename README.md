# Usage

-   Install dependencies: `npm install`
-   Start development server and watch for changes: `npm start`
-   Build for production: `npm run build`
