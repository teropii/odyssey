import React from 'react';
import PropTypes from 'prop-types';

import WebRadio from './WebRadio';

class WebRadioContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = { currentlyPlayingRadioId: null };
    this.radios = [];
  }

  handlePlaybackStarted = startedRadioId => {
    const { currentlyPlayingRadioId } = this.state;
    const isRadioCurrentlyPlaying = currentlyPlayingRadioId !== null;

    // Stop currently playing radio if necessary.
    isRadioCurrentlyPlaying && this.radios[currentlyPlayingRadioId].stop();
    
    this.setState({ currentlyPlayingRadioId: startedRadioId });

    // Update document title with the new radio name.
    const startedRadio = this.props.radios.find(radio => radio.id === startedRadioId);
    startedRadio.name && this.setTitle(startedRadio.name);
  };

  handlePlaybackStopped = stoppedRadioId => {
    this.setState({ currentlyPlayingRadioId: null });
    this.setTitle('odyssey');
  };

  setTitle = title => {
    document.title = title;
  };

  getControlledRadioComponent = (name, url, id) => {
    const storeRadioRef = radio => this.radios[id] = radio;
    const eventConfig = {
      id: id,
      playbackStarted: this.handlePlaybackStarted,
      playbackStopped: this.handlePlaybackStopped
    };

    return (
      <WebRadio
        name={name}
        url={url}
        onEvent={eventConfig}
        key={id}
        ref={storeRadioRef}
      />
    );
  };

  render() {
    const controlledRadioComponents = this.props.radios.map(({ name, url, id }) =>
      this.getControlledRadioComponent(name, url, id));
    
    return (
      <main className="web-radio-container">
        {controlledRadioComponents}
      </main>
    );
  }
}

WebRadioContainer.propTypes = {
  radios: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    name: PropTypes.string
  })).isRequired
};

export default WebRadioContainer;
