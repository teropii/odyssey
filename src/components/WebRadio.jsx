import React from 'react';
import PropTypes from 'prop-types';

import './WebRadio.scss';

class WebRadio extends React.Component {
  constructor(props) {
    super(props);
  
    this.audio;
    this.status = Object.freeze({
      IDLE: Symbol('idle'),
      LOADING: Symbol('loading'),
      PLAYING: Symbol('playing')
    });

    this.state = { status: this.status.IDLE };
  }

  handleClick = () => {
    this.state.status === this.status.PLAYING ? this.stop() : this.tryToPlay();
  };

  stop = () => {
    // Removing the source stops the stream.
    // If this is not done, the stream will continue to buffer in the background.
    this.audio.setAttribute('src', '');
    this.setState({ status: this.status.IDLE });
    this.onPlaybackStopped();
  };

  tryToPlay = () => {
    this.audio.setAttribute('src', this.props.url);
    const audioPromise = this.audio.play();

    this.setState({ status: this.status.LOADING });

    if (audioPromise !== undefined) {
      audioPromise.then(response => {
        this.setState({ status: this.status.PLAYING });
        this.onPlaybackStarted();
      }).catch(error => {
        this.audio.pause();
        this.setState({ status: this.status.IDLE });
      });
    } else {
      // IE and Edge don't return a promise.
      this.setState({ status: this.status.PLAYING });
      this.onPlaybackStarted();
    }
  };

  onPlaybackStarted = () => {
    const { onEvent } = this.props;
    if (onEvent && onEvent.playbackStarted) {
      const { id, playbackStarted } = onEvent;
      playbackStarted(id);
    }
  };

  onPlaybackStopped = () => {
    const { onEvent } = this.props;
    if (onEvent && onEvent.playbackStopped) {
      const { id, playbackStopped } = onEvent;
      playbackStopped(id);
    }
  };

  getIcon = () => {
    const { status } = this.state;
    const isPlaying = status === this.status.PLAYING;
    const isLoading = status === this.status.LOADING;

    const iconName = isPlaying ? 'pause' : 'play_arrow';

    if (isLoading) {
      return (
        <div className="spinner-container">
          <span className="spinner" />
          <i className="material-icons icon">close</i>
        </div>
      );
    } else {
      return <i className="material-icons icon">{iconName}</i>;
    }
  };

  componentDidMount() {
    this.props.autoPlay && this.tryToPlay();
  }

  render() {
    const { name, url } = this.props;
    const isPlaying = this.state.status === this.status.PLAYING;
    const storeAudioRef = audio => this.audio = audio;

    const classes = 'web-radio' + (isPlaying ? ' playing' : '');

    return (
      <div onClick={this.handleClick} className={classes}>
        <h1>{name}</h1>
        {this.getIcon()}
        <audio ref={storeAudioRef} src={url} preload="none" />
      </div>
    );
  }
}

WebRadio.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string,
  autoPlay: PropTypes.bool,
  onEvent: PropTypes.shape({
    id: PropTypes.number.isRequired,
    playbackStarted: PropTypes.func,
    playbackStopped: PropTypes.func
  })
};

export default WebRadio;
