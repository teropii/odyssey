import React from 'react';

import WebRadioContainer from '../components/WebRadioContainer';

import './App.scss';

import radios from '../radios.json';

const App = () => {
  return (
    <div className="app">
      <header>
        <h1>Odyssey</h1>
        <div className="line"></div>
      </header>
      <WebRadioContainer radios={radios} />
    </div>
  );
};

export default App;
